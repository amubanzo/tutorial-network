# Tutorial Network

### Introduction

This project provides a Hyperledger Composer based network based on the Hyperledger Composer Developer Tutorial and Trade Network sample.

The goal of this project is to experiment with a Hyperledger Composer network setup alongside a customized composer-rest-server module.

### How to start the Rest API

In your terminal, run the following commands:

1. `npm install`
2. `export secretOrKey='your key here'`
3. `./deploy_crs.sh`
4. `npm run start-server`
